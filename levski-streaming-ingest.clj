;; add to project.clj dependecies:
;;    [org.apache.spark/spark-streaming_2.11 "2.0.2" :exclusions [log4j]]
;;    [org.apache.spark/spark-streaming-kafka-0-8_2.11 "2.0.2"]
;; import https://github.com/gorillalabs/sparkling/blob/develop/tmp/streaming.clj as levski.streaming

;; in levski repl
(require '[cenx.levski.kafka.streaming :as ss]
         '[clojure.core.async :as async]
         '[sparkling.conf :as conf]
         '[sparkling.core :as spark])


;;======== option 1 =======

(def my-ch (async/chan 10))

(defn ingest-fn
  [e]
  (println (str e))
  (cenx.levski.kafka.core/ingest {:value e} my-ch))

(defn my-fn
  [e]
  (->> (spark/values e)
       (spark/coalesce 1)
       (spark/map-to-pair (fn [e] (spark/tuple (:timestamp e) e)))
       (spark/sort-by-key)
       (spark/values)
       (spark/foreach ingest-fn)))

(def c (-> (conf/spark-conf)
           (conf/master "local")
           (conf/app-name "streaming-ingest")))

(def ssc (ss/streaming-context c 5000))

(def dstream (ss/kafka-dstream ssc))

(.foreachRDD dstream (sparkling.function/void-function my-fn))

(.start ssc)

(async/thread
 (loop []
   (println "waiting for message....")
   (when-let [msg (async/<!! my-ch)]
     (println "found message!")
     (recur))))

;;======== option 2 =======

(require '[cenx.levski.kafka.streaming.streaming :as ss]
         '[sparkling.conf :as conf]
         '[sparkling.core :as spark])

(def c (-> (conf/spark-conf)
           (conf/master "local")
           (conf/app-name "streaming-ingest")))

(def ssc (ss/streaming-context c 5000))

(def dstream (ss/kafka-dstream ssc))

(.foreachRDD dstream (sparkling.function/void-function
                      (fn
                        [l]
                        (->> (spark/values l)
                             (spark/map (fn [e]
                                          (-> (#'cenx.levski.kafka.core/parse-message {:value e})
                                              (clojure.set/rename-keys {:metric-name :metric
                                                                        :value :vdouble})
                                              (assoc :idfamily "vim"
                                                     :week 0
                                                     :vlong nil
                                                     :vtext nil)
                                              (dissoc :version))))
                             (cenx.plataea.cassandra.rdd/save-to-cassandra "vnf_data")))))

(.start ssc)
;;ingest (trumpet traffic)
(.stop ssc)